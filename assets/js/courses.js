let adminUser = JSON.parse(localStorage.getItem("isAdmin"))		//localStorage = cookies; either true, false or undefined
let modalButton = document.querySelector("#adminButton")
let cardFooter;
//enableCourse Function
let enableCourse = function(id){
	fetch('https://shara-capstone-2.herokuapp.com/api/courses/enable/'+id,{
		method: 'PATCH',
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${localStorage.getItem("token")}`
		},
	})
	.then(res => window.location.reload())
}


//conditional renderring
if(adminUser === false || !adminUser){
	modalButton.innerHTML = null
}else{
	modalButton.innerHTML =
	`
	<div class="col-md-2 offset-md-10">
		<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
	</div>
	`
}

//Fetch the courses from our API and display it to users in front-end
fetch('https://shara-capstone-2.herokuapp.com/api/courses',{
	headers: {
		"Content-Type": "application/json",
		authenticated: localStorage.getItem("token") ? true : false,
		Authorization: `Bearer ${localStorage.getItem("token")}`
	},
})
.then(res => res.json())				//res = response
.then(data => {
	// console.log(data)

	let courseData;

//if the number of courses is less than 1 diplay no courses available
	if (data.length <1) {
		courseData = "No course available"
	}else{
		//iterate the course collection and display each course
		courseData = data.map(course => {

			if(adminUser===false || !adminUser){
				cardFooter = 
				`
					<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton">Select Course</a>
				`
			}else{
				//check if the user is an admin, display the edit and delete button if they are
				cardFooter = 
				`<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton">Edit</a>`
				cardFooter += course.isActive ? `<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block dangerButton">Disable Course</a>` : `<button onclick="enableCourse('${course._id}')" class="btn btn-success text-white btn-block dangerButton">Enable Course</button>`
			}

			return (
				`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									${course.price}
								</p>
							</div>

							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
			)

		}).join("")
	}

	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData

})