let params = new URLSearchParams(window.location.search)
let token = localStorage.getItem('token')
let courseId = params.get('courseId')

// console.log(courseId)

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")
let enrolleesTableTbody = document.querySelector("#enrolleesTable > tbody")

fetch(`https://shara-capstone-2.herokuapp.com/api/courses/${courseId}`)
  .then((res) => res.json())
  .then((data) => {

	for(enrollee of data.enrollees){
		fetch("https://shara-capstone-2.herokuapp.com/api/users/details?userId="+enrollee.userId,{
			headers: {
				'Content-TYpe': 'application/json',
				'Authorization' : `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(user => {
			let tr = document.createElement('tr')
			let tdFirstName = document.createElement('td')
			let tdLastName = document.createElement('td')
			tdFirstName.innerHTML = user.firstName
			tdLastName.innerHTML = user.lastName
			tr.append(tdFirstName)
			tr.append(tdLastName)
			enrolleesTableTbody.append(tr)
		})
	}

  	name.value = data.name
  	price.value = data.price
  	description.value = data.description

  	document.querySelector('#editCourse').addEventListener('submit', (e) => {
  		e.preventDefault()

  		let courseName = name.value
  		let desc = description.value
  		let priceValue = price.value

  		fetch('https://shara-capstone-2.herokuapp.com/api/courses', {
  			method: "PUT",
  			headers: {
  				'Content-TYpe': 'application/json',
  				'Authorization' : `Bearer ${token}`
  			},
  			body: JSON.stringify({
  				courseId: courseId,
  				name: courseName,
  				description: desc,
  				price: priceValue
  			})
  		})
  		.then(res => res.json())
  		.then(data => {
  			if(data === true){
  				window.location.replace('./courses.html')
  			}else{
  				alert("Ooops..Something went wrong!")
  			}
  		})
  	})

  });

