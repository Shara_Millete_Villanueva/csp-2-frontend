//returns the query string part of the url 
// console.log(window.location.search)

// instantiate a URLSearchParams onbject so we can access specific parts of the query string
let params = new URLSearchParams(window.location.search)

// get returns the value of the key passed as an argument, then we store it in a value
let token = localStorage.getItem("token"); 

let courseId = params.get('courseId')

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
//displays info of a specific course when user clicks on that course

fetch(`https://shara-capstone-2.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(course => {
	// console.log(course)
	courseName.innerHTML = course.name; 
	courseDesc.innerHTML = course.description; 
	coursePrice.innerHTML = course.price; 
	if(localStorage.getItem('isAdmin') == "false"){
		enrollContainer.innerHTML = course.enrollees.find(enrollee => enrollee.userId == localStorage.getItem('id')) ? `ENROLLED` : `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`
	
	if(course.enrollees.find(enrollee => enrollee.userId == localStorage.getItem('id')) == undefined){
		document.querySelector('#enrollButton').addEventListener("click", () => {
	
			fetch("https://shara-capstone-2.herokuapp.com/api/users/enroll", {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId,
				}),
		})
			.then((res) => {
				return res.json()
			})
			.then((data) =>{
				// console.log(data)
				if (data === true) {
					alert("Thank you for enrolling!")
					// window.location.replace("./courses.html")
				}else{
					alert("Ooops.. Something went wrong!")
				}
			})
		})
	}
	}
})

